\documentclass{beamer}
\mode<presentation>
{
  \usetheme{Warsaw}
  \setbeamercovered{transparent}
  \usecolortheme[RGB={0,90,0}]{structure}
}

\usepackage{etex}
\usepackage[main=brazilian,english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage[alf]{abntex2cite} % Citações padrão ABNT
\usepackage{listings}
\usepackage[absolute,overlay]{textpos} % Posicionamento absoluto
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\usepackage{booktabs}

\title[A Linguagem Go: Alto Desempenho e Programação Paralela]
{A Linguagem de Programação Go:}

\subtitle
{Estudos de Caso em Alto Desempenho\\e Programação Paralela}

\author{Diogo Doreto}

\institute[UNIFESP - ICT]
{
  Orientador: Prof. Dr. Alvaro Fazenda
  \vspace{1\baselineskip}\\
  Universidade Federal de São Paulo -- UNIFESP\\
  Instituto de Ciência e Tecnologia\\
  Bacharelado em Ciência da Computação
}

\date{Junho/2015 - Trabalho de Conclusão de Curso}

\subject{Talks}

% Logo UNIFESP
\pgfdeclareimage[height=0.8cm]{university-logo}{./images/logo-unifesp.pdf}
\logo{\pgfuseimage{university-logo}}

% Table of contents at the beginning of each subsection:
\AtBeginSubsection[]
{
  \begin{frame}<beamer>{Conteúdo}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

% Remove controles do slide
\setbeamertemplate{navigation symbols}{}

% itemize bullets
\setbeamertemplate{itemize items}[square]
\setbeamertemplate{itemize subitem}[circle]
\setbeamertemplate{bibliography item}{\textbullet{} }

% ToC bullets
\setbeamertemplate{section in toc}
{\leavevmode\leftskip=2ex%
  \llap{%
    \usebeamerfont*{section number projected}%
    \usebeamercolor{section number projected}%
    \begin{pgfpicture}{-1ex}{0ex}{1ex}{2ex}
      \color{bg}
      \pgfpathcircle{\pgfpoint{0pt}{.65ex}}{1.7ex}
      \pgfusepath{fill}
      \pgftext[base]{\color{fg}\inserttocsectionnumber}
    \end{pgfpicture}\kern2.25ex%
  }%
  \inserttocsection\par}
\setbeamertemplate{subsection in toc}
  {\leavevmode\leftskip=2em$\bullet$\hskip1em\inserttocsubsection\par}

% Aparência dos blocos
\setbeamertemplate{blocks}[default]

% Definição da linguagem Go
\lstdefinelanguage{go}{
  morekeywords={break,default,func,interface,select,case,defer,go,map,struct,chan,else,goto,package,switch,const,fallthrough,if,range,type,continue,for,import,return,var,uint8,uint16,uint32,uint64,int8,int16,int32,int64,float32,float64,complex64,complex128,byte,rune,uint,int,uintptr,string,bool,true,false},
  sensitive=true,
  morecomment=[l]{//},
  morecomment=[s]{/*}{*/},
  morestring=[b]",
  morestring=[b]',
  tabsize=4,
}

% Aparência das listings
\renewcommand\lstlistingname{Listagem}
\lstset{
  basicstyle=\ttfamily\scriptsize,
  numbers=none,
  breaklines=true,
  breakatwhitespace=true,
  tabsize=4,
  showstringspaces=false,
  language=c,
  texcl=true
}

% Configuração dos gráficos
\pgfplotsset{compat=1.12}
%\selectcolormodel{gray}
\usetikzlibrary{patterns}
\pgfplotstableset{
  every head row/.style={before row=\toprule,after row=\midrule},
  every last row/.style={after row=\bottomrule},
  columns/C/.style={fixed, fixed zerofill,precision=2,column type=r},
  columns/Go/.style={fixed, fixed zerofill,precision=2,column type=r},
  columns/Go-B/.style={fixed, fixed zerofill,precision=2,column type=r},
  columns/C Speed up/.style={fixed, fixed zerofill,precision=2,column type=r},
  columns/Go Speed up/.style={fixed, fixed zerofill,precision=2,column type=r},
}

\newcommand{\comparecgo}[1]{
  \begin{scriptsize}
    \pgfplotstablegetelem{0}{C}\of{#1} \edef\cbase{\pgfplotsretval}
    \pgfplotstablegetelem{0}{Go}\of{#1} \edef\gobase{\pgfplotsretval}
    \begin{minipage}[b]{0.49\linewidth}
      \begin{tikzpicture}
        \begin{axis}[
            ybar,
            ymin={0},
            width=\textwidth,
            xlabel={Threads},
            ylabel={Tempo (ms)},
            symbolic x coords={1,2,4,8},
            xtick=data,
          ]
          \addplot table [y=C] {#1};
          \addplot table [y=Go] {#1};
          \legend{C, Go}
        \end{axis}
      \end{tikzpicture}
    \end{minipage}
    %\hspace{2mm}
    \begin{minipage}[b]{0.49\linewidth}
      \begin{tikzpicture}
        \begin{axis}[
            width=\textwidth,
            xlabel={Threads},
            ylabel={Speed up},
            ymax={4},
            symbolic x coords={1,2,4,8},
            xtick=data,
            ytick={0,...,5},
            grid=major,
            legend style={at={(0.02,0.98)},anchor=north west},
          ]
          \addplot table[y expr=\cbase  / \thisrow{C} ] {#1};
          \addplot table[y expr=\gobase / \thisrow{Go}] {#1};
          \legend{C, Go}
        \end{axis}
      \end{tikzpicture}
    \end{minipage}
    \vskip8pt plus.5fill
    \pgfplotstabletypeset[
      columns={Threads,C,Go,C Speed up,Go Speed up},
      create on use/C Speed up/.style={create col/expr={\cbase / \thisrow{C}}},
      create on use/Go Speed up/.style={create col/expr={\gobase / \thisrow{Go}}},
    ]{#1}
  \end{scriptsize}
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Conteúdo}
  \tableofcontents
\end{frame}


\section{Programação Concorrente}

\subsection{Introdução}

\begin{frame}{Motivação}
  \begin{itemize}
    \item Expectativa fixa: \emph{Hardware} mais potente de tempos em tempos
    \item Problema: Aumentar frequência do \emph{clock} do processador
    \item Solução: Inserir mais núcleos
  \end{itemize}
\end{frame}

\begin{frame}{Introdução}
  \begin{block}{Definição: Concorrência} 
    Aspiração de muitos à posse ou obtenção de algo
  \end{block}
  \begin{itemize}
    \item \textbf{Muitos} $\to$ Processos; \textbf{Algo} $\to$ Recursos
  \end{itemize}
  \vskip0pt plus.5fill
  \begin{itemize}
    \item Concorrente $\neq$ Paralelo
  \end{itemize}
\end{frame}

\subsection{Sistemas de Memória}

\begin{frame}{Sistemas de Memória Distribuída}
  \begin{itemize}
    \item Computadores separados interligados por uma rede
    \item Processos diferentes; Envio de mensagens
  \end{itemize}

  \begin{figure}
    \caption{Exemplo de arquitetura distribuída}
    \begin{center}
      \includegraphics[width=0.7\linewidth]{./images/distribuido.pdf}
    \end{center}
    \scriptsize{Fonte: \citeonline[p. 27]{Dongarra:2003:SPC:941480}}
  \end{figure}
\end{frame}

\begin{frame}{Sistemas de Memória Compartilhada}
  \begin{itemize}
    \item Espaço de endereçamento de memória comum às CPUs
    \item Subclassificação por custo de tempo de acesso
    \begin{itemize}
      \item UMA - \emph{Uniform Memory Access}
      \item NUMA - \emph{Non-Uniform Memory Access}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Sistemas de Memória Compartilhada}{Exemplos}
  \begin{figure}
    \caption{Arquiteturas típicas de memória compartilhada}
    \begin{center}
      \includegraphics[width=0.9\linewidth]{./images/shared-memory.pdf}
    \end{center}
    \scriptsize{(a) e (b) Arquiteturas de acesso uniforme à memória (UMA).\\(c) Arquitetura de acesso não-uniforme à memória (NUMA). Fonte: \citeonline{Kumar:2002:IPC:600009}.}
  \end{figure}
\end{frame}

\subsection[Programação em Memória Compartilhada]{Programação de Máquinas com Memória Compartilhada}

\begin{frame}{Programação de Máquinas com Memória Compartilhada}
  \begin{quote}
    Processadores de memória compartilhada são populares devido aos seus modelos de programação simples e gerais, que permitem um ágil desenvolvimento de softwares paralelos que suportam o compartilhamento de código e dados \cite{Tomasevic:1994:HAC:623259.623875}.
  \end{quote}
\end{frame}

\begin{frame}[containsverbatim]{Exemplo Serial}
  \begin{lstlisting}[language=c]
int res1 = 0, res2 = 0;

int main(void) {
    tarefa_a(&res1);
    tarefa_b(&res2);
    tarefa_final(res1, res2);
    return 0;
}

void tarefa_a(int * contador) {
    int i, j, x;
    for (i = 0; i < 4; i++) {
        printf("Tarefa A\n");
        for (j = 0; j < 10000; j++) x += i;
        (*contador)++;
    }
}
  \end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]{Pthreads}
  \begin{itemize}
    \item \emph{Threads}: Mais leves e eficientes do que \emph{fork}
    \item POSIX Threads: Padronização da API
  \end{itemize}
  \begin{lstlisting}
#include <pthread.h>

int main(void) {
    pthread_t thread1, thread2;
    pthread_create(&thread1, NULL, 
        (void *) tarefa_a, (void *) &res1);
    pthread_create(&thread2, NULL, 
        (void *) tarefa_b, (void *) &res2);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    tarefa_final(res1, res2);
    return 0;
}
  \end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]{OpenMP}
  \begin{itemize}
    \item Diretivas e funções para especificar paralelismo em alto-nível
    \item Facilita escrita de código e adaptação em código existente
  \end{itemize}
  \begin{lstlisting}[language=c]
#include <omp.h>

int res1 = 0, res2 = 0;

int main(void) {
#pragma omp parallel sections shared(res1, res2)
    {
        tarefa_a(&res1);
#pragma omp section
        tarefa_b(&res2);
    }
    tarefa_final(res1, res2);
    return 0;
}
  \end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]{OpemMP}{Outros Exemplos}
  \begin{itemize}
    \item Exemplo de soma de vetores:
  \end{itemize}
  \begin{lstlisting}[language=c]
#pragma omp parallel for private(i)
for (i = 0; i < qtd_elementos; i++) {
    resultado[i] = vet_a[i] + vet_b[i];
}
  \end{lstlisting}
  
  \begin{itemize}
    \item Exemplo de somatório:
  \end{itemize}
  \begin{lstlisting}[language=c]
double soma = 0.0;
#pragma omp parallel for private(i) reduction(+:soma)
for (i = 0; i < qtd_elementos; i++) {
    soma += vetor[i];
}
  \end{lstlisting}
\end{frame}


\section{A Linguagem de Programação Go}
\subsection{O Projeto}

\begin{frame}{O que é o projeto Go?}
  \begin{textblock*}{20mm}[1,0](119mm,30mm)
    \includegraphics[width=20mm]{./images/gopherbw.png}
  \end{textblock*}
  \begin{itemize}
    \item Projeto de código aberto (BSD)
    \item Liberado em 2007, versão 1.0 em 2012
    \item Consiste de:
      \begin{itemize}
        \item Especificação da linguagem
        \item \emph{Runtime} (\emph{garbage-collector}, \emph{scheduler}, \ldots)
        \item Compilador GC
        \item Biblioteca Padrão 
        \item Documentação
        \item Conjunto de ferramentas (\texttt{build}, \texttt{run}, \\ \texttt{test}, \texttt{get}, \texttt{doc}, \texttt{fmt}, \texttt{pprof})
      \end{itemize}
    \item http://www.golang.org
  \end{itemize}
\end{frame}

\begin{frame}{Os Criadores}
  \begin{textblock*}{30mm}[1,0](125mm,35mm)
    \includegraphics[width=30mm]{./images/Bell_Laboratories_logo.pdf}
  \end{textblock*}
  \begin{textblock*}{16mm}[1,0](118mm,47mm)
    \includegraphics[width=16mm]{./images/plan9bunnywhite.jpg}
  \end{textblock*}
  \begin{itemize}
    \item Ken Thompson
      \begin{itemize}
        \item \textbf{Turing Award}: Implementação do UNIX \\ (junto de Dennis Ritchie)
        \item Criador da Linguagem B
        \item Participação na criação da linguagem C
      \end{itemize}
    \item Rob Pike\footnote[frame]{Feitos realizados em conjunto de Ken Thompson na Bell Labs}
      \begin{itemize}
        \item Sistemas operacionais Plan 9 e Inferno
        \item UTF-8
      \end{itemize}
    \item Robert Griesemer
  \end{itemize}
\end{frame}

\begin{frame}{Motivação}
  \begin{itemize}
    \item Criada na Google, para resolver problemas da Google
    \item Grandes \emph{hardwares}, grandes \emph{softwares}, grandes equipes
  \end{itemize}
  \begin{figure}
    \caption{Datacenter na Google}
    \begin{center}
      \includegraphics[width=0.7\linewidth]{./images/datacenter.jpg}
    \end{center}
    \scriptsize{Fonte: http://talks.golang.org/2012/splash.article}
  \end{figure}
\end{frame}

\begin{frame}{A Linguagem Go}
  \begin{textblock*}{23mm}[1,0](122mm,35mm)
    \includegraphics[width=23mm]{./images/gopher-project.png}
  \end{textblock*}
  \begin{itemize}
    \item Go é:
    \begin{itemize}
      \item Concorrente
      \item Compilada
      \item Estaticamente tipada
      \item \emph{Garbage-collected}
    \end{itemize}
    \item Go busca ser:
    \begin{itemize}
      \item Eficiente
      \item Escalável
      \item Produtiva
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Usuários}
  \begin{center}
    \includegraphics[width=\linewidth]{./images/logos.png}
  \end{center}
  \scriptsize{Fonte: https://github.com/golang/go/wiki/GoUsers}
\end{frame}

\subsection{Concorrência em Go}

\begin{frame}[containsverbatim]{Concorrência em Go}
\begin{textblock*}{49mm}[1,0](122mm,63mm)
    \footnotesize{\emph{Do not communicate by sharing memory; instead, share memory by communicating.}}
  \end{textblock*}
  \begin{itemize}
    \item Suporte à concorrência a nível de linguagem
    \item Aplica uma variante do modelo \emph{Communicating Sequential Processes} \cite{Hoare:1978:CSP:359576.359585}
    \item Canais de comunicação são \emph{first-class}
    \item A abordagem é a composição de funções independentes executando concorrentemente (\emph{goroutines})
  \end{itemize}

  \begin{lstlisting}[language=go]
func falar(c chan string) {
  c <- "Ola"
}

func main() {
  c := make(chan string)
  go falar(c)
  fmt.Println(<-c)
}
  \end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]{Exemplo: Fibonacci Concorrente}
  \begin{lstlisting}[language=go]
func fibonacci() <-chan int {
	ch := make(chan int)
	a, b := 0, 1
	go func() {
		for {
			a, b = b, a+b
			ch <- a
		}
	}()
	return ch
}

func main() {
	fib := fibonacci()

	for i := 0; i < 10; i++ {
		fmt.Printf("%v ", <-fib)
	}
} 
  \end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]{Exemplo: Mutex de Leitura/Escrita}
  \begin{lstlisting}[language=go]
type SafeMap struct {
    values map[string]int64,
    mtx sync.RWMutex
}

func (s *SafeMap) Load(key string) int64 {
    s.mtx.RLock()
    defer s.mtx.RUnlock()
    return s.values[key]
}

func (s *SafeMap) Store(key string, val int64) {
    s.mtx.Lock()
    defer s.mtx.Unlock()
    s.values[key] = val
}

//Exemplo de uso:
valor := sm.Load("chave")
sm.Store("chave", valor)
  \end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]{Exemplo: Operações Atômicas (incorreto)}
\begin{lstlisting}[language=go]
type Watchdog struct{ last int64 }

func (w *Watchdog) KeepAlive() {
  w.last = time.Now().UnixNano() // First conflicting access.
}

func (w *Watchdog) Start() {
  go func() {
    for {
      time.Sleep(time.Second)
      // Second conflicting access.
      if w.last < time.Now().Add(-10*time.Second).UnixNano() {
        fmt.Println("No keepalives for 10 seconds. Dying.")
        os.Exit(1)
      }
    }
  }()
}
  \end{lstlisting}
  \scriptsize{Fonte: http://golang.org/doc/articles/race\_detector.html}
\end{frame}

\begin{frame}[containsverbatim]{Exemplo: Operações Atômicas (correto)}
\begin{lstlisting}[language=go]
type Watchdog struct{ last int64 }

func (w *Watchdog) KeepAlive() {
  atomic.StoreInt64(&w.last, time.Now().UnixNano())
}

func (w *Watchdog) Start() {
  go func() {
    for {
      time.Sleep(time.Second)
      if atomic.LoadInt64(&w.last) < time.Now().Add(-10*time.Second).UnixNano() {
        fmt.Println("No keepalives for 10 seconds. Dying.")
        os.Exit(1)
      }
    }
  }()
}
  \end{lstlisting}
  \scriptsize{Fonte: http://golang.org/doc/articles/race\_detector.html}
\end{frame}

\subsection{Programação Distribuída em Go}

\begin{frame}{Programação Distribuída em Go}
  \begin{itemize}
    \item Biblioteca Padrão:
      \begin{itemize}
        \item Comunicação (TCP, UDP, HTTP, RPC, \ldots)
        \item Serialização (XML, JSON, Base64, ASN.1, \ldots)
      \end{itemize}
    \item Comunidade:
      \begin{itemize}
        \item Etcd\footnote[frame]{https://github.com/coreos/etcd}
      \end{itemize}
  \end{itemize}
\end{frame}


\section{Estudos de Caso}

\begin{frame}<beamer>{Conteúdo}
  \tableofcontents[currentsection,currentsubsection]
\end{frame}
  
\begin{frame}{Especificações do Sistema de Testes}
  \begin{itemize}
    \item Ubuntu 15.04 de 64 bits
    \item Intel\textsuperscript{\textregistered} Core\texttrademark{} i7-4770 CPU @ 3.40GHz \\(8 núcleos, 4 reais)
    \item 1 x 8GiB DDR3 @ 1600MHz
    \item Versões:
      \begin{itemize}
        \item GCC 4.9.2 (OpenMP 4.0)
        \item Go 1.4.2
      \end{itemize}
    \item Testes executados com \texttt{perf stat -d -r 100}
  \end{itemize}
\end{frame}

\subsection[Algoritmo FTCS]{Algoritmo Forward-Time Central-Space}
\begin{frame}{O Algoritmo FTCS}
  \begin{itemize}
    \item Resolução numérica de equações de calor
    \item Resolução numérica de equações diferenciais parciais
  \end{itemize}
  \begin{block}{Exemplo}
\[\frac{\partial u}{\partial t} = \alpha \frac{\partial^2 u}{\partial x^2}\]

\[\frac{u^{n+1}_i - u^n_i}{\Delta t} = \frac{\alpha}{\Delta x^2} \left(u^n_{i+1} - 2u^n_i + u^n_{i-1}\right)\]

\[u^{n+1}_i = u^n_i + r\left(u^n_{i+1} - 2u^n_i + u^n_{i-1}\right) \qquad \text{, com } r = \frac{\alpha \Delta t}{\Delta x^2}\]
  \end{block}
\end{frame}

\begin{frame}{FTCS: Resultados}
  \begin{center}
    \comparecgo{./plotdata/ftcs.dat}
  \end{center}
\end{frame}

\subsection[Algoritmo ln(2)]{Algoritmo ln(2) por Integração Numérica}
\begin{frame}[containsverbatim]{O Algoritmo $ln(u)$}{Cálculo por integração numérica}
  Baseado na seguinte identidade matemática:

  \[\int \left(u-1\right) \cdot \frac{1}{x} \cdot dx = ln(u) \qquad , u > 1\]

  Podemos aplicar o método do trapézio para integração numérica e obter uma aproximação do resultado, conforme pseudocódigo:

  \begin{lstlisting}[keywords={funcao,para,retorna}]
funcao ln(u, iteracoes) {
    passo = (u-1) / iteracoes
    para (i = 0; i < iteracoes; i++) {
        x = 1 + i*passo
        soma = soma + 0.5 * (1/x + 1/(x+passo))
    }
    retorna soma * passo
}
  \end{lstlisting}
\end{frame}

\begin{frame}{$ln(2)$: Resultados}{$10^8$ iterações}
  \begin{center}
    \comparecgo{./plotdata/ln2.dat}
  \end{center}
\end{frame}

\subsection{Algoritmo Desvio Padrão}
\begin{frame}{Desvio Padrão}
  \begin{block}{Definição}
    \[\sigma = \sqrt{\frac{1}{N} \sum^N_{i=1} \left(x_i-\mu\right)^2} \qquad , \text{onde } \mu = \frac{1}{N} \sum^N_{i=1} x_i\]
  \end{block}
  \begin{itemize}
    \item Quantifica dispersão em um conjunto de dados
  \end{itemize}
\end{frame}

\begin{frame}{Desvio Padrão: Resultados}{$10^8$ elementos}
  \begin{center}
    \comparecgo{./plotdata/desvp.dat}
  \end{center}
\end{frame}

\section*{}

\begin{frame}[t]{Conclusão}
  \begin{itemize}
    \item Computação científica não está nos objetivos da linguagem Go
    \item Bom desempenho em operações aritméticas em CPU, sem uso intensivo de dados em memória
    \item Desenvolvimento da linguagem segue ativo
  \end{itemize}
  
  Extensões:

  \begin{itemize}
    \item Testes da linguagem Go em sistemas distribuídos
    \item Novos testes para identificar causa real de baixo desempenho
  \end{itemize}
\end{frame}

\begin{frame}{Bibliografia}
  \scriptsize{\bibliography{references.bib}}
\end{frame}

\end{document}

